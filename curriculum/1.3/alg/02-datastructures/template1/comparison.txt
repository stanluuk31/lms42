For each of the listed data structures and each of the listed operations, state how the time
it takes to complete the operations depends on the number of items in the data structure.
You can choose from these options, where `n` is the number of items in the data structure

  - O(1) -- Time doesn't depend on the number of items in the list.
  - O(log n) -- Time goes up with the number of items in the list, but less than linearly.
  - O(n) -- Time goes up with the number of items in the list linearly.
  - O(n*log(n)) -- Time goes up with the number of items in the list a bit more than linearly.
  - O(n*n) -- Time goes up with the number of items in the list quadratically.
  - N/A -- Not applicable, in case the data structure does not support this operation.

If you don't know, you can do some experiments to find out!

list:
    add an element: 
    insert an element at the start: 
    remove an element with a specific value: 
    remove the last element: 
    remove the first element: 
    check if a certain value is contained: 

set:
    add an element: 
    insert an element at the start: 
    remove an element with a specific value: 
    remove the last element: 
    remove the first element: 
    check if a certain value is contained: 

tuple:
    add an element: 
    insert an element at the start: 
    remove an element with a specific value: 
    remove the last element: 
    remove the first element: 
    check if a certain value is contained: 

deque:
    add an element: 
    insert an element at the start: 
    remove an element with a specific value: 
    remove the last element: 
    remove the first element: 
    check if a certain value is contained: 

dict: (note! different operations)
    add a key+value pair: 
    get the value for a key: 
    get the key for a value: 
    remove a pair by its key: 
