# You should not need to change this file. (But feel free to do so anyway!)

import sys
from glob import glob
from sjs_tokenizer import tokenize
from sjs_tree import print_tree, RunData
from sjs_parser import Parser

# A few little helper functions.
def ask_choice(prompt, choices):
    choices = list(choices)
    assert choices
    while True:
        print(prompt)
        for index, choice in enumerate(choices):
            print(f"  {index+1}. {str(choice).capitalize()}")
        index = input("> ")
        if index.isdigit() and 1 <= int(index) <= len(choices):
            return choices[int(index)-1]

def print_section_title(title):
    print(f"\n\n{'='*80}\n{' '+title+' ':{'='}^80}\n{'='*80}\n", flush=True)

# Ask what we need to do.
filename = ask_choice('Input file?', sorted(glob("*.sjs"))) if len(sys.argv) < 2 else sys.argv[1]
mode = ask_choice('Mode?', ['tokenize', 'parse', 'run']) if len(sys.argv) < 3 else sys.argv[2]

# Read the input file.
with open(filename) as file:
    source = file.read()

# Get a list of tokens!
tokens = tokenize(source)

# Filter out all tokens that are irrelevant for parsing.
tokens = [token for token in tokens if token.kind not in {'comment', 'whitespace'}]

# Print tokens
print_section_title("Tokenizing")
for token in tokens:
    print(token)
if mode == 'tokenize':
    quit()

# Parse into an AST and print it.
print_section_title("Parsing")
tree = Parser(tokens).parse()
print_tree(tree)
if mode == 'parse':
    quit()

# Run the program.
print_section_title("Running")
tree.run(RunData())
