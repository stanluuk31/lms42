name: Cryptography
goals:
    crypto: 1
resources:
    -
        link: https://www.youtube.com/watch?v=jhXCTbFnK8o
        title: Crash Course Computer Science - Cryptography
        info: A short introductory video to cryptography.
    -
        link: https://www.youtube.com/watch?v=cqgtdkURzTE&t=680s
        title: Cryptography For Beginners
        info: This video is a lot more in-depth. You don't need to know all of this to complete this assignment, but it may be worth knowing anyhow!
    -
        link: https://pynacl.readthedocs.io/en/latest/public/
        title: PyNaCl - Public Key Encryption
        info: Documentation on how to do public key encryption using the PyNaCl Python library.
    -
        link: https://pynacl.readthedocs.io/en/latest/secret/
        title: PyNaCl - Secret Key Encryption
        info: Documentation on how to do shared key encryption using the PyNaCl Python library.
assignment:
    - |
        Provided is a simple client/server application. It works like this:

        - The client starts and ask its user for a secret.
        - It sends the secret to an already running server.
        - The server sees if it had already seen this exact secret before, and lets the client know. If it didn't know the secret yet, it adds it to its file of secrets.
        - The client just tells the user if the secret was already known, or just added.

        The applications work fine(ish), but given that they're dealing with important secrets, they're wildly insecure! Your job, of course, is to apply some cryptography to fix that.

    -
        title: Encryption at rest
        0: Nothing more than obfuscation.
        1: Wrong algorithm and insecure.
        2: Wrong algorithm but still mostly secure, or right algorithm but insecure.
        3: Right algorithm, sloppy but secure implementation.
        4: Right algorithm (SecretBox), perfect implementation.
        text: |
            In case hackers manage to break into the server, they can just copy the secrets file and publish it on Wikileaks. Apply some [encryption at rest](https://en.wikipedia.org/wiki/Data_at_rest#Encryption) to prevent this from being so easy. In this case, it's okay to have the key (as a hexadecimal string) in the server's source code.

            Use the simplest sufficient form of encryption from PyNaCl.

    -
        title: Encrypt messages in transit
        0: Nothing more than obfuscation.
        1: Wrong algorithm and insecure.
        2: Wrong algorithm but still mostly secure, or right algorithm but insecure.
        3: Right algorithm, sloppy but secure implementation.
        4: Right algorithm (SealedBox), perfect implementation.
        text: |
            We want to make sure that no [sniffer](https://www.computerhope.com/jargon/s/sniffing.htm) or [man-in-the-middle](https://www.veracode.com/security/man-middle-attack) can learn the secrets being sent to the server.

            The client should send the secret such that only the real (!) server can decrypt it. Not even other clients may be able to decrypt it.

            Use the simplest sufficient form of encryption from PyNaCl. (Hint: only use one key to encrypt and one key to decrypt )

    -
        title: Encrypt responses in transit
        0: Nothing more than obfuscation.
        1: Wrong algorithm and insecure.
        2: Wrong algorithm but still mostly secure, or right algorithm but insecure.
        3: Right algorithm, sloppy but secure implementation.
        4: Right algorithm (Box), perfect implementation.
        text: |
            For this assignment, please work on a copy of your client and server.

            Modify your protocol such that hackers will not be able to find out whether the secret some client contributed was new or already know to the server. This extra requirements takes a bit of creativity, and you may have to change your solution to the previous objective to get it done.
            (Hint: use 2 keys to encrypt and decrypt your message)
