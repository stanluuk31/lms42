name: Example Mapping
description: Create an example map, a domain model and wire-frames for a case description.
goals:
    requirements: 2
    datamodeling: 1
    wireframing: 1
    device-design: 1
days: 1
pair: true
resources:
    - 
        link: https://www.markdownguide.org/basic-syntax
        title: Basic Markdown Syntax
        info: Nearly all Markdown applications support the basic syntax outlined in this document. There are minor variations between Markdown viewers.
    -
        link: https://plantuml.com/ie-diagram
        title: PlantUML - Entity Relationship Diagram
        info: Documentation on ERDs in PlantUML

assignment:
    - Introduction: |
        We are developing our skills in discovering possible answers to the question: what should we build In the last assignment you were given the example map and had to design a datamodel and a wire-frame. The example map was already given. In this assignment you will have to develop the example map yourself.

    - Assignment: |
        In this assignment we develop a functional design for a *card swap auction* case. The technique will we use is called *Example Mapping*. Watch the video provided in the resources to get a better understanding of what this is.

        In the template folder you will find a case description for an card swap by auction system in the file `case-card-swap-auction.md`. Please develop the example map and the datamodel for this case. The file `case-card-swap-auction.wt` contains a template for developing the wire-frames for this case.  

    - Example mapping:
      -
        link: https://youtu.be/VwvrGfWmG_U?si=_NuJqCodWrTUtXOy&t=659
        title: Introducing Example Mapping
        info: In this video the booking of a train ticket is worked out using example mapping (watch from 10:59 until 26:09).
      -
        text: |
          The template in `case-card-swap-auction.md` show a number of stories that splits up what you need to specify. A (user) story is simply a label for a conversation you need to have about a particular aspect of the application. 

          For each story you need to create the *rules* and *examples* that specify the behavior of the system relevant for that story.
          
          * Examples should illustrate the rules.
          * Examples should check different instances of applying the rule. You should not include examples that do not check anything new.
          * Rules should summarize the examples.
          * Rules should be exact enough for you to implement them in a computer program
          

        0: Rules and Examples are not relevant for the case described
        4: Consistent rules and relevant examples that specify the desired behavior
        map:
          requirements: 2

    - Formulate questions:
      -
        text: |
          After you have created the rules and examples reflect on whether everything has sufficiently been specified. In `case-card-swap-auction.md` there is a section *Questions* for which you need to write down: 
          - Questions about whether the rules match the provided case description
          - Questions about what is missing in the provided case description
          - If there are any contradicting rules

        0: Little to no questions have been created
        4: Relevant and critical questions have been created
        map:
          requirements: 1

    - Domain model:
      -
        link: https://www.youtube.com/watch?v=jtWj-rreoxs
        title: Map out a Domain Model
        info: Introduction into how map entities from use cases to a domain model.
      -
        text: |
          Create a domain model (logical ERD including the attribute type) for the provided case description:

          - Create the domain model using PlantUML (see the file `case-card-swap-auction.md` in the template folder).
          - Your domain model should be fully consistent with the case description and your wireframes (next objective).

        0: Domain model does not give a clear impression of the application to be build
        4: Domain contains all entities with proper relationship (including correct cardinality).
        map:
          datamodeling: 1

    - Wireframes:
      - 
        link: https://uxplanet.org/desktop-vs-mobile-design-the-only-rule-you-must-know-8ac71714450a
        title: "Desktop vs Mobile design: The only RULE you must know!"
        info: A blog explaining the difference between designing for mobile vs designing for desktop.
      -
        text: |
          Create **low fidelity** wireframes for your application, demonstrating how the end product might work (but not what it will look like). Your design should show which visual elements are found in your application screens and how the user can interact with these elements.

          Your tasks:
          - Create a wireframe for every screen in your **desktop** application (in `case-card-swap-auction.wt` or Quant-UX). Do *not* use standard filler text (like *Lorem ipsum*), but try to come up with some realistic example data. That allows you to verify that all data fields actually make sense, and helps viewers understand how the application will be used in practice.
          - For every screen you should explain (*in a note next to the screen*) the following:
            1. Which elements are interactive (though this should be obvious in your wireframe).
            2. For each interactive element the effect it has in the application. For example a "Save" button in your application will save information in an applications database. You should describe these actions. Although this "Save" example is straightforward some actions a not (for example a push notification that is sent to a different user when a message is created in the application). Buttons/links that refer to another WireText screen generally don't need any further description.
          - Your wireframes should be fully consistent with the case description and your domain model.

        0: Wireframes do not give a clear impression of the application to be build
        4: Clear wireframes including intuitive navigation between pages.
        map:
          wireframing: 1
          device-design: 1
