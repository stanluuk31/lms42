# These should be all the imports you need:
from flask import Flask, render_template, request
from random import randint

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True

# Run using `poetry install && poetry run flask run --reload`

with open('words.txt', encoding='utf-8') as file:
    words = file.read().strip().split("\n")

def guess_to_hint(guess, secret):
    """Given a `guess` and a `secret` word as strings, it returns a list with one tuple for
    each letter in the guess, where the first item is the letter, and the second item is one
    of the strings `correct`, `wrong` or `misplaced`, describing what applies for that letter.
    """
    result = []
    for idx, letter in enumerate(guess):
        actual = secret[idx]
        if actual == letter:
            result.append((letter, 'correct'))
        elif letter in secret:
            result.append((letter, 'misplaced'))
        else:
            result.append((letter, 'wrong'))
    return result

@app.route("/", methods=['GET'])
def index():
    """When the user visit the root of the website with a GET request and starts/resume the game."""
    # TODO Implement the objective when the user visit the root of the website with a GET method (default visiting).
    return "Hello web!"

@app.route("/", methods=['POST'])
def handle_submit():
    """When the user post a attempt for the game."""
    # TODO Implement the attempts and checks of objective 4
    # TODO process the data that the users submitted thru the form (request)
    return "Attempt done"

@app.route("/history", methods=['GET'])
def show_history():
    """Show history of the attempts the user made."""
    return "Here is your game history"
