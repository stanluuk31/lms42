hello_world: # memory address of the start of the string
    "Hello world!\n"
hello_world_end: # memory address of the end of the string

current_address: # memory address of a 16-bit integer
    int16 hello_world # 16-bit integer that holds the address of the next character to print

main: # entry point and loop address
    write8 **current_address # write the 8-bit value that the integer at current_address refers to
    add16 *current_address 1 # increment the integer at current_address
    if<16 *current_address hello_world_end  # only run the next instruction if the value in the integer at current_address is smaller than the address specified by hello_world_end
    set16 *ip main
    set16 *ip 0
    
